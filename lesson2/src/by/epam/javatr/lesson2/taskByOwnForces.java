package by.epam.javatr.lesson2;

import java.util.Scanner;

/**
 *  ��� ������� ��� ���������������� �������. ���� 2
 *  @author Sergii Kotov
 */
public class taskByOwnForces {
	// global for class
	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		
		System.out.println("__2__");
		task_2();
		System.out.println("__3__");
		task_3();
		System.out.println("__4__");
		System.out.println(task_4());
		System.out.println("__5__");
		System.out.println(task_5());
		System.out.println("__6__");
		System.out.println(task_6());
		System.out.println("__7__");
		task_7();
		System.out.println("__8__");
		task_8();
		System.out.println("__9__");
		task_9();
		System.out.println("__10__");
		task_10();
		System.out.println("__11__");
		task_11();
		
		System.out.println("__12__");
		task_12(args);

		System.out.println("__13__");
		double a= readDoubleVals("������� a");
		double b= readDoubleVals("������� b");
		double c= readDoubleVals("������� c");
		double d= readDoubleVals("������� d");
		System.out.println(task_13(1,a,b,c,d));
		System.out.println(task_13(2,a,b,c,d));
		
		System.out.println("__14__");
		task_14(a,b,c);
		System.out.println("__15__");
		task_15();
		System.out.println("__16__");
		task_16(9.64);
		System.out.println("__17__");
		System.out.println(task_17(26));
	}
	//-------------------------------------------------------------------------------------
	/**
	 *  ����� ��� ���������� ������ � ���������� (double)
	 *  @param  text - message to show for request
	 *  @return double - version for input doubles   
	 *  @author Sergii Kotov
	 */
	static private double readDoubleVals(String text) {
		System.out.println(text);
		if(scanner.hasNextDouble()) {
			return scanner.nextDouble();
		}else{
			scanner.next();
			System.out.println("�� ����� �� �����");
			return Double.NaN;
		}
	}
		
	/**
	 *  ����� ��� ���������� ������ � ���������� (long)
	 *  @param  text - message to show for request
	 *  @return long - version for input longs
	 *  @author Sergii Kotov
	 */
	static private long readIntVals(String text) {
		System.out.println(text);
		if(scanner.hasNextInt()) {
			return scanner.nextInt();
		}else{
			scanner.next();
			System.out.println("�� ����� �� �����");
			return Integer.MIN_VALUE;
		}
				
	}
	//-------------------------------------------------------------------------------------
	
	/**
	 * task2. �������� �� ������� ���� ���, ����� � �������.
	 */
	static private void task_2 () {
		System.out.println("����� ������ ����������. ���. 222-111-111");
	}
	/**
	 * task3. �������� �� ����� ��������� �����.
	 */
	static private void task_3 () {
		StringBuilder txt = new StringBuilder()
			    .append("������� ����� ���, � ��������� ��������� �������, ���� � ����� � �������� ���������,")
			    .append("�������� ��� ������� �����, ����� ���� ���� ��� � ����� ���������� �� ���. ������� ���� ") 
			    .append("����� ��������� �������� � ����� ��������� �����, ����������� �� ����� � ������� �� ")			    		
			    .append("������ ����, ������� ����� ���� ���������� ���� �");
		System.out.println(txt.toString()); 
	}
	/**
	 * task4. ���������� ���������� � ������� �������������� ������������ �� ���� �������.
	 */
	static private double task_4 () {
		double a= readDoubleVals("������� ����� ������� ������");
		double b= readDoubleVals("������� ����� ������� ������");
		return Math.sqrt(a*a+b*b);
	}
	/**
	 * task5. �������� ��������� ���������� ����� ������ ���������.
	 */
	static private double task_5 () {
		double a= readDoubleVals("������� ������ ���������");
		double b= readDoubleVals("������� ������ ���������");
		double c= readDoubleVals("������� ������ ���������");
		double d= readDoubleVals("������� ��������� ���������");
		return a+b+c+d;
	}
	/**
	 * task6. ��������� ����������� � ������������ a,b,c ,��������� �������� ��������� a2-(b-c)2+ln(b2+1).
	 */
	static private double task_6 () {
		double a= readDoubleVals("������� a");
		double b= readDoubleVals("������� b");
		double c= readDoubleVals("������� c");
		return Math.pow(a,2)-Math.pow((b-c),2)+Math.log1p(Math.pow(b,2)+1);
	}
	/**
	 * task7. ��������� ������ ��� ����� a � b, ���� � > b, 
	 * �� ��������� ������ ������ ����� � � ������� ����� b+c. 
	 * ���� �=b, �� ��������� �������� ����� �������. 
	 * ���� �<b, �� ��������� ������ ������ ����� �, ������� ����� a+b+c, ������� �� ����� ����� a+b+c � ����� ������ ���!�.
	 * 
	 */
	static private void task_7 () {
		double a= readDoubleVals("������� a");
		double b= readDoubleVals("������� b");
		int k=Double.compare(a,b);
		if (k>0){
			double c= readDoubleVals("������� c");
			System.out.println(b+c);
		} else {
			if (Double.compare(a,b)==0){
				System.out.println("�����");
			} else {
				double c= readDoubleVals("������� c");
				System.out.println(a+b+c);
				System.out.println("����� ���!");
			}
		}	
	}
	
	/**
	 * task8. ��������� ������ ��� ����� a � b, ���� a=b, �� ������� �� ����� ����� ������ �����
	 * ���!�, � ��������� ������ ������ ������ ����� � � ������� �� ����� 3 ������ :
	 * -�������� ��������� a+b+c
	 * -�������� ��������� a2 + b2
	 * -���� ������� ���������� �������
	 */
	static private void task_8 () {
		double a= readDoubleVals("������� a");
		double b= readDoubleVals("������� b");
		int k=Double.compare(a,b);  
		if (k==0){
			System.out.println("����� ����� ���!");
		} else {
				double c= readDoubleVals("������� c");
				System.out.println(a+b+c);
				System.out.println(Math.pow(a,2)+Math.pow(b,2));
				System.out.println("� �� ����� ������");
				}
	}
	/**
	 * task 9. ��������� ����������� ���������� �����, ����� ������ ����� ����� � ������������
	 * � ��������� �����������. ���������� ���������� ������, ����� ������� 3-� �
	 * ���������� �����, ������ ������� <3
	 */
	static private void task_9 () {
	int n= (int)readIntVals("������� ���������� �����");
	int k;
	int quantEven=0;
	int sumModIs3=0;
	int quantModuleLess3=0;
	for (int i=0; i<n;i++) {
		k=(int)readIntVals("������� "+(i+1)+"-� �����");
		if (k%2==0){
			quantEven++;
		}	
		if (k%3==0){
			sumModIs3+=k;
		}
		if (Math.abs(k)<3){
			quantModuleLess3++;
		}	
	}
	System.out.println("�� "+n+" ����� "+quantEven+" ������, "+quantModuleLess3+" ������ ������� ������ 3. � ����� ������� 3 ����� "+sumModIs3);
	}
	/**
	 * task 10. ��������� ������ ���������� ����� N, � ����� N ������������ �����. ���������
	 * ������� ������������ ����� � ����������, ������� ����� ��������� ����� ������ � >15.
	 */
	static private void task_10 () {
		int n= (int)readIntVals("������� ���������� �����");
		long k;
		int quant=0;
		for (int i=0; i<n;i++) {
			// ������ "�������" � �� ���, ������� ��� �������� - � ����������� � � ����������� ����
			//k=Math.round( readDoubleVals("������� "+(i+1)+"-� �����") );
			k= (long)readDoubleVals("������� "+(i+1)+"-� �����");
			if ((k%2==0) &&(k>15)){
				quant++;
			}		
		}
		System.out.println("�� "+n+" ����� "+quant+" ������ � ������ 15");
	}
	/**
	 * task 11. ��������� ����������� ���������� �����, ������ ����� ����� � ������������ �
	 * ���������� ����������� ,����������, ������� ����� ������ 15 ��� <2, ���� �����
	 * ����� �����, ������� ������� �� 5 � �������� 4. ��������� ������� �� �����.
	 */
	static private void task_11 () {
		int n= (int)readIntVals("������� ���������� �����");
		int k;
		int quant1=0;
		int sum2=0;
		for (int i=0; i<n;i++) {
			k=(int)readIntVals("������� "+(i+1)+"-� �����");
			if ((k>15) || (k<2)) {
				quant1++;
			}	
			if (k%5==4) {
				sum2++;
			}
		}
		System.out.println("�� "+n+" ����� "+quant1+" ������ 15 ��� ������ 2, � ����� ����� ������� ������� �� 5 � �������� 4 ����� "+sum2);
	}
	/**
	 * task 12	������� ��������� ����� ����� ����� ��� ���������� ��������� ������, ��������
	 * �� ����� � ������ ���������� �� �������
	 */
	static private void task_12 (String[] ar) {
		int sm=0;
		boolean f=false;
		for(int i=0; i<ar.length; i++){
			try {
				sm+= Integer.parseInt(ar[i]);
			}catch(NumberFormatException ex){ 
				f=true;
				break;
			}
		}
		if (f){
			System.out.println("�� ���������� �������� ���������� ���������� ������");
		} else {
			System.out.println("����� ���������� ���������� ������ ����� "+sm);
		}
	}
	/**
	 * task 13	��������� �������� ��������� �� ������� (��� ���������� ��������� �������������� ��������)
	 * ���������� ���������� � ���������� �� ��������� �������
	 * @param numFormula ����� �� ���� ������ ���������. 
	 * @return one of two function values
	 */
	static private double task_13 (int numFormula, double a, double b, double c, double d) {
		if (numFormula==1){
			return ( b+ Math.sqrt(Math.pow(b, 2)+4*a*c) )/( 2*a )  -  Math.pow(a, 3)*c + b;
		} else {
			return (a/c) * (b/c) - (a*b-c)/(c*d);
		}
	}
	/**
	 * task 14	���� ��� �������������� �����. �������� � ������� �� �� ���, �������� �������
	 * ��������������, � � ��������� ������� � �������������.
	*/
	static private double task_14_1 (double a) {
		return (Double.compare(a,0)>=0)?Math.pow(a,2):Math.pow(a,4);
	}
	static private void task_14 (double a, double b, double c) {
		System.out.println(" a="+task_14_1(a)+" b="+task_14_1(b)+" c="+task_14_1(c));
	}
	
	/**
	 * task 15 ��������� ��������� ��� ���������� �������� ������� F(x) �� ������� [�, b] �	����� h. 
	 * ��������� ����������� � ���� �������, ������ ������� ������� � ��������
	 * ���������, ������ - ��������������� �������� �������
	*/
	static private void task_15 () {
		double a= readDoubleVals("������� a");
		double b= readDoubleVals("������� b");
		// ���� ���������� �������
		if (Double.compare(a,b)>0){
			double f=b;
			b=a;
			a=f;
		}
		double h= readDoubleVals("������� h");
		System.out.println("	arg		funct");
		while (Double.compare(a,b)<=0) {
			System.out.println("	"+a+"	"+(2*Math.tan(a/2)+1));
			a+=h;
		}
		// �������� �� ����� ���������
		if (Double.compare(a-h,b)<0){
			System.out.println("	"+b+"	"+(2*Math.tan(b/2)+1));
		}
	}
	/**
	 * task 16 ��������� ����� ���������� � ������� ����� ������ � ���� �� ��������� ������� R
	 * @param r radius
	 */
	static private void task_16 (double r) {
		System.out.println("��� ������� "+r);
		System.out.println("����� ���������� = "+r*2*Math.PI);
		System.out.println("������� ����� = "+r*r*Math.PI);
	}
	/**
	 * task 17 ��������� �������� �������
	 * @param x function parameter
	 */
	static private double task_17 (double x) {
		if (Double.compare(x,-3)<=0) {
			return 9.0;
		}else {
			if (Double.compare(x,3)>0) {
				return 1/ (x*x+1);
			} else {
				return Double.NaN;
			}
		}
	}
	
}
