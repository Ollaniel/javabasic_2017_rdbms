package by.epam.javatr.rdbms;

import java.util.Scanner;

public class task3 {
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		int r=0;
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("������� �������������� �����: ");
		if(scanner.hasNextInt()) {
			r = scanner.nextInt();
		}else{
			scanner.next();
			System.out.println("�� ����� �� �����");
		}
		
		long m=multiplyNumberDigits(r);
		if (m>=0) {
			System.out.println("������������ ���� ��� ����� "+r+" ���������� "+m+".");
		} else {
			System.out.println("����� �� ��������������.");
		}
	}
	
	static private long multiplyNumberDigits(int r) {
		if ((r>9999)|| (r<1000)) {
			return -1;
		} else {
			int firstD=r%10;
			int lastD =r/1000;
			int secD =(r%100-firstD)/10;
			int thirdD= r/100-lastD*10;  
			return firstD*secD*thirdD*lastD;
		}
	}
	
}
